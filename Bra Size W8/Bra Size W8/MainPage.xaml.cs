﻿using Bra_Size;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bra_Size_W8
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly int CM_INDEX = 0;

        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Wird aufgerufen, wenn diese Seite in einem Rahmen angezeigt werden soll.
        /// </summary>
        /// <param name="e">Ereignisdaten, die beschreiben, wie diese Seite erreicht wurde. Die
        /// Parametereigenschaft wird normalerweise zum Konfigurieren der Seite verwendet.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Calculate(object sender, RoutedEventArgs e)
        {
            BraSize braSize = new BraSize(BelowCm(), BustCm());
            sizeEU.Text = braSize.SizeEU().ToString();
            sizeBEF.Text = braSize.SizeBEF().ToString();
            sizeUSA.Text = braSize.SizeUSA().ToString();
            sizeUK.Text = braSize.SizeUK().ToString();
            sizeAUS.Text = braSize.SizeAUS().ToString();
            sizeI.Text = braSize.SizeI().ToString();
        }

        private double BelowCm()
        {
            double below = Double.Parse(belowInput.Text);
            if (belowUnit.SelectedIndex != CM_INDEX)
                below = Units.InchToCm(below);
            return below;
        }

        private double BustCm()
        {
            double bust = Double.Parse(bustInput.Text);
            if (bustUnit.SelectedIndex != CM_INDEX)
                bust = Units.InchToCm(bust);
            return bust;
        }
    }
}
