﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bra_Size
{
    class BraSize
    {
        private double below;
        private double bust;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="below">Below breast extend in cm</param>
        /// <param name="bust">On breast extend in cm</param>
        public BraSize(double below, double bust)
        {
            this.below = below;
            this.bust = bust;
        }

        public BraSizeTuple SizeEU()
        {
            return new BraSizeTuple(Round(below, 5), CupSizeEU(bust - below));
        }

        public BraSizeTuple SizeBEF()
        {
            BraSizeTuple sizeEU = SizeEU();
            return new BraSizeTuple(sizeEU.Band + 15, sizeEU.Cup);
        }

        public BraSizeTuple SizeUSA()
        {
            // XXX: "plus four" method applied here
            return new BraSizeTuple(Round(Units.CmToInch(below), 2) + 4, CupSizeUSA(bust - below));
        }

        public BraSizeTuple SizeUK()
        {
            return new BraSizeTuple(SizeUSA().Band, CupSizeUK(bust - below));
        }

        public BraSizeTuple SizeAUS()
        {
            BraSizeTuple sizeUK = SizeUK();
            return new BraSizeTuple(sizeUK.Band - 24, sizeUK.Cup);
        }

        public BraSizeTuple SizeI()
        {
            BraSizeTuple sizeEU = SizeEU();
            BraSizeTuple sizeUK = SizeUK();
            // XXX: Italy actually uses 2.5 cm steps (and not 1 inch = 2.54 cm)
            return new BraSizeTuple(sizeEU.Band / 5 - 12, sizeUK.Cup);
        }

        private int Round(double value, int factor)
        {
            return (int)Math.Round(value / factor) * factor;
        }

        private string CupSizeEU(double bustBelowDiff)
        {
            string[] chars = { "AA", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            Debug.Assert(chars.Length == 27);
            return LinearScale(chars, 11, 63, bustBelowDiff);
        }

        private string CupSizeUK(double bustBelowDiff)
        {
            string[] chars = { "AA", "A", "B", "C", "D", "DD", "E", "F", "FF", "G", "GG", "H", "HH", "J", "JJ", "K", "KK", "L", "LL", "M", "MM", "N" };
            Debug.Assert(chars.Length == 22);
            return LinearScale(chars, 3.5, 24.5, Units.CmToInch(bustBelowDiff));
        }

        private string CupSizeUSA(double bustBelowDiff)
        {
            string[] chars = { "AA", "A", "B", "C", "D", "DD/E", "DDD/F", "DDDD/G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U" };
            Debug.Assert(chars.Length == 22);
            return LinearScale(chars, 3.5, 24.5, Units.CmToInch(bustBelowDiff));
        }

        private string LinearScale(string[] options, double min, double max, double value)
        {
            int pos = (int)Math.Round((value - min) / (max - min) * (options.Length - 1));
            pos = Math.Max(0, Math.Min(options.Length - 1, pos));
            return options[pos];
        }
    }
}
