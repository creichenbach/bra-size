﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Bra_Size
{
    public partial class MainPage : PhoneApplicationPage
    {
        private readonly int CM_INDEX = 0;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private void Calculate(object sender, RoutedEventArgs e)
        {
            BraSize braSize = new BraSize(BelowCm(), BustCm());
            sizeEU.Text = braSize.SizeEU().ToString();
            sizeBEF.Text = braSize.SizeBEF().ToString();
            sizeUSA.Text = braSize.SizeUSA().ToString();
            sizeUK.Text = braSize.SizeUK().ToString();
            sizeAUS.Text = braSize.SizeAUS().ToString();
            sizeI.Text = braSize.SizeI().ToString();
            Panorama.DefaultItem = Panorama.Items[1]; // XXX: Nicer way?
        }

        private double BelowCm()
        {
            double below = Double.Parse(belowInput.Text);
            if (belowUnit.SelectedIndex != CM_INDEX)
                below = Units.InchToCm(below);
            return below;
        }

        private double BustCm()
        {
            double bust = Double.Parse(bustInput.Text);
            if (bustUnit.SelectedIndex != CM_INDEX)
                bust = Units.InchToCm(bust);
            return bust;
        }
    }
}