﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bra_Size
{
    class Units
    {
        private static readonly double INCH_CM_FACTOR = 2.54;

        public static double InchToCm(double inch)
        {
            return inch * INCH_CM_FACTOR;
        }

        public static double CmToInch(double cm)
        {
            return cm / INCH_CM_FACTOR;
        }
    }
}
