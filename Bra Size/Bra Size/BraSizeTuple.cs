﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bra_Size
{
    class BraSizeTuple
    {
        public BraSizeTuple(int band, string cup)
        {
            this.Band = band;
            this.Cup = cup;
        }

        internal int Band
        {
            get;
            private set;
        }

        internal string Cup
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return Band + Cup;
        }
    }
}
